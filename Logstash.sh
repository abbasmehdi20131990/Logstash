#!/bin/bash/
###https://techexpert.tips/elasticsearch/logstash-installation-ubuntu-linux/
##mettre a jour linux
sudo apt-get update
sudo apt-get -y install default-jre apt-transport-https wget gnupg ntpdate
sudo groupadd --system logstash
sudo useradd -s /sbin/nologin --system -g logstash logstash
##changer la variable d'environnement java
sudo update-alternatives --config java
sudo apt install -y openjdk-11-jdk
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
sudo apt update 
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo add-apt-repository "deb https://artifacts.elastic.co/packages/7.x/apt stable main"
sudo apt-get update
sudo apt-get upgrade
sudo apt-get -y install logstash
sudo systemctl enable logstash
sudo systemctl stop logstash
sudo tee /etc/logstash/conf.d/logstash-syslog.conf<<EOF
input {
  tcp {
    port => 5000
    type => syslog
  }
  udp {
    port => 5000
    type => syslog
  }
}

filter {
  if [type] == "syslog" {
    grok {
      match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}" }
      add_field => [ "received_at", "%{@timestamp}" ]
      add_field => [ "received_from", "%{host}" ]
    }
    date {
      match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
    }
  }
}

output {
  elasticsearch { hosts => ["192.168.100.9:9200"] }
  stdout { codec => rubydebug }
}
EOF
sudo systemctl start logstash
####Logstash - Sending Syslog messages
sudo apt-get update
sudo apt-get -y install rsyslog
##Stop the RSyslog service.
sudo systemctl stop rsyslog
#Edit the Syslog configuration file.
sudo sed -e 's/*.* @192.168.100.9:5000'/etc/rsyslog.d/50-default.conf
##Add the following lines at the end of the file.
##the RSyslog service.
sudo systemctl start rsyslog
